#ifndef LIGHTDS_NETSERVER_HPP
#define LIGHTDS_NETSERVER_HPP

#include <thread>
#include <mutex>
#include <condition_variable>
#include <memory>
#include <vector>
#include <list>
#include <functional>
#include <boost/asio.hpp>
#include "netmsg.hpp"

namespace LightDS
{
	class NetServer
	{
	protected:
		static const size_t bufferSize = 2048;

	protected:
		struct Connection
		{
			boost::asio::ip::tcp::socket	socket;
			std::vector<char>				recvBuffer;

			NetMessage						netMsg;

			NetServer &server;
			std::list<Connection *>::iterator iter;

			Connection(NetServer &server) : socket(server.service), recvBuffer(bufferSize), server(server)
			{
				{
					std::lock_guard<std::mutex> lock(server.mtxAliveConn);
					if (!server.srvAlive)
						socket.close();
					server.listAliveConn.push_front(this);
					iter = server.listAliveConn.begin();
				}
				netMsg.setMsgHandler(std::bind(&NetServer::onRecvMsg, &server, this, std::placeholders::_1));
			}

			~Connection()
			{
				std::lock_guard<std::mutex> lock(server.mtxAliveConn);
				server.listAliveConn.erase(iter);
				if (server.listAliveConn.empty())
					server.cvAliveConn.notify_all();
			}
		};
	public:
		typedef std::function<std::string(
			const std::string &/*msg*/, const std::string &/*ip*/, std::uint16_t /*port*/)> reqHandler_t;

	public:
		NetServer(boost::asio::ip::tcp::endpoint endpoint) :
			srvAlive(true),
			work(new boost::asio::io_service::work(service))
		{
			using namespace boost::asio;

			acceptor.reset(new ip::tcp::acceptor(service, endpoint));
			std::shared_ptr<Connection> conn(new Connection(*this));

			acceptor->async_accept(
				conn->socket, 
				[this, conn](const boost::system::error_code &error_code) { 
					onAccept(conn, error_code); 
			});
		}
		~NetServer()
		{
			std::unique_lock<std::mutex> lock(mtxAliveConn);
			srvAlive = false;
			for (Connection *conn : listAliveConn)
			{
				boost::system::error_code ec;
				conn->socket.close(ec);
			}
			acceptor.reset();

			if(!listAliveConn.empty())
				cvAliveConn.wait(lock);

			work.reset();
			
			for (auto &thread : worker)
				thread.join();
		}

		void setReqHandler(reqHandler_t handler)
		{
			reqHandler = handler;
		}

		boost::asio::ip::tcp::endpoint getLocalEndpoint() const
		{
			return acceptor->local_endpoint();
		}

		void Run(unsigned int nThread = std::thread::hardware_concurrency() * 2)
		{
			std::lock_guard<std::mutex> lock(mtxAliveConn);
			if (!srvAlive)
				return;

			for (unsigned int i = 0; i < nThread; i++)
			{
				worker.emplace_back([this]()
				{
					service.run();
				});
			}
		}

	protected:
		void onAccept(std::shared_ptr<Connection> conn, const boost::system::error_code &error_code)
		{
			if (error_code)
			{
				//TODO: Handle Error
				return;
			}
			conn->socket.async_receive(
				boost::asio::buffer(conn->recvBuffer),
				[this, conn](const boost::system::error_code &error_code, size_t recvLen)
				{
					onRecv(conn, recvLen, error_code);
				});
			std::shared_ptr<Connection> next_conn(new Connection(*this));
			acceptor->async_accept(
				next_conn->socket,
				[this, next_conn](const boost::system::error_code &error_code) {
				onAccept(next_conn, error_code);
			});
		}
		void onRecv(std::shared_ptr<Connection> conn, size_t recvLen, const boost::system::error_code &error_code)
		{
			if (error_code)
			{
				return;
			}
			conn->netMsg.onRecv(conn->recvBuffer, recvLen);
			conn->socket.async_receive(
				boost::asio::buffer(conn->recvBuffer),
				[this, conn](const boost::system::error_code &error_code, size_t recvLen)
				{
					onRecv(conn, recvLen, error_code);
				});
		}

		void onRecvMsg(Connection *conn, const std::string &msg)
		{
			auto remote = conn->socket.remote_endpoint();
			std::shared_ptr<std::string> response(new std::string(
				NetMessage::sendMsg(
					reqHandler(msg, remote.address().to_string(), remote.port()))));
			//conn->socket.async_send(boost::asio::buffer(*response), [response](boost::system::error_code, size_t) {});
			boost::asio::async_write(conn->socket, boost::asio::buffer(*response), [response](boost::system::error_code, size_t) {});
		}

	protected:
		std::mutex mtxAliveConn;
		std::condition_variable cvAliveConn;
		std::list<Connection *> listAliveConn;
		bool srvAlive;

		boost::asio::io_service							service;
		std::unique_ptr<boost::asio::io_service::work>	work;
		std::unique_ptr<boost::asio::ip::tcp::acceptor>	acceptor;

		std::list<std::thread> worker;

		reqHandler_t reqHandler;
	};
}

#endif